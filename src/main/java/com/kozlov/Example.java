package com.kozlov;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
@MyAnnotation(course = 3,age = 19, Address = "World")
public class Example {
    private Logger logger1 = LogManager.getLogger(Example.class);
    @MyAnnotation(age = 5, course = 0, Address = "s")
    public String string = "Hello its empty string";
    @MyAnnotation(name = "Anton", age = 5, course = 4, Address = "House")
    String sayHi = "HI";
    private int number = 5;
    protected double doubleNumber = 0;
    Example(){ }
    Example(int a, int v){
        int intArgument = a + v;
    }
    @MyAnnotation(name = "Misha", age = 19, course = 2,Address = "Opera")
    public String FirstPublic(String b){
        return "String is"+ " " + b;
    }
    protected int SecondProtected(int a, int b){
        return a*b;
    }
    private double ThirdPrivate(double a, int c){
        return a-c;
    }
    public void myMethod(String string, int ... a){
        logger1.info(string);
        for(int b : a){
            logger1.info(" " + b);
        }
        logger1.info("\n");
    }
    public void myMethod(String ... a){
        for(String b : a){
            logger1.info(" " + b);
        }
        logger1.info("\n");
    }
}
