package com.kozlov;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface MyAnnotation {
    String name() default "Artem";
    int age();
    int course();
    String Address();
}
