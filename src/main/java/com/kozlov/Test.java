package com.kozlov;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Scanner;
class Test {
    private Logger logger = LogManager.getLogger(Test.class);
    private Example example = new Example();
    private Class c = example.getClass();
    private Scanner scan = new Scanner(System.in, "UTF-8");
    void start(){
        infoTest();
        valueWithoutType();
    }
    private void infoTest(){
        Class[] argTypesString = new Class[] { String[].class };
        Class[] argTypesInt = new Class[] { String.class,int[].class };
        Field[] allFields = c.getDeclaredFields();
        for(Field currentField: allFields){
            MyAnnotation annotation = currentField.getAnnotation(MyAnnotation.class);
            if(currentField.isAnnotationPresent(MyAnnotation.class)){
                logger.info(currentField.getName() + "\n");
                logger.info("@" );
                logger.info(annotation.annotationType().getSimpleName() +
                        "(name = "+ annotation.name()+ ", age = " + annotation.age() + ", course = " +
                        annotation.course() + ", adress = " + annotation.Address()+")" + "\n");
            }
        }
        try {
            Method firstMethod = c.getDeclaredMethod("FirstPublic", String.class);
            logger.info(firstMethod.invoke(example, "Artem") + "\n");

            Method secondMethod = c.getDeclaredMethod("SecondProtected", int.class, int.class);
            logger.info(secondMethod.invoke(example, 5 ,6)+ "\n");

            Method thirdMethod = c.getDeclaredMethod("ThirdPrivate", double.class, int.class);
            thirdMethod.setAccessible(true);
            logger.info(thirdMethod.invoke(example,3.14,3) + "\n");

            Method myMethod = c.getDeclaredMethod("myMethod",argTypesInt);
            myMethod.invoke(example,"Numbers ", new int[]{5,9,8});

            Method myMethodOnlyString = c.getDeclaredMethod("myMethod", String[].class);
            myMethodOnlyString.invoke(example,  new Object[]{new String[]{"a", "b", "c"}});
        } catch (IllegalAccessException | NoSuchMethodException
                | InvocationTargetException | IllegalArgumentException e){
            e.printStackTrace();
        }
    }
    private void valueWithoutType(){
        logger.info("Input var name and var value: \n");
        String field = scan.next();
        if(field.equals("string")){
            setValue(field, scan.nextLine());
        } else {
            if(field.equals("number")){
                setValue(field,scan.nextInt());
            }
            else {
                if(field.equals("doubleNumber")){
                    setValue(field,scan.nextDouble());
                }
            }
        }
    }
    private <T> void setValue(String name, T value){
        try {
            Field field = c.getDeclaredField(name);
            field.setAccessible(true);
            field.set(example, value);
        } catch (NoSuchFieldException | IllegalAccessException
                | IllegalArgumentException e) {
        e.printStackTrace();
    }
}
}
