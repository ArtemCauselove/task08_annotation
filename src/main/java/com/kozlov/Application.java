package com.kozlov;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
public class Application {
    public static void main(String[] args) {
        Test test = new Test();
        Logger logger = LogManager.getLogger(Test.class);
        Class c = Example.class;
        Field[] allFields = c.getDeclaredFields();
        logger.info("\t Class name : " + c.getSimpleName() + "\n");
        logger.info("\t\t Fields \n");
        for (Field f: allFields) {
            logger.info(f.getModifiers() + " " + f.getName() + "" + Arrays.toString(f.getDeclaredAnnotations())+ "\n");
        }
        Method[] allMethods = c.getDeclaredMethods();
        logger.info("\t\tMethods \n");
        for(Method m : allMethods){
            logger.info(  " " + m.getModifiers() + " return type : " + m.getReturnType() + " "+ m.getName() +
                    Arrays.toString(m.getParameterTypes()) + Arrays.toString(m.getDeclaredAnnotations()) +"\n");
        }
        logger.info("\t\t Conctructors \n");
        Constructor[] allConstrustorc = c.getDeclaredConstructors();
        for(Constructor current : allConstrustorc){
         logger.info(current.getName() + " " + Arrays.toString(current.getParameterTypes()) + "\n");
        }
        test.start();
    }
}
